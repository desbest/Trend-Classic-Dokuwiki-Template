# Trend Classic dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:trendclassic)

![trend classic theme screenshot](https://i.imgur.com/TM4yrvq.png)